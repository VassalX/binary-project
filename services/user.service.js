const UsersRepo = require("../repositories/user.repository");

const isUser = (user) => {
    if (
        user &&
        user.id &&
        user.first_name &&
        user.last_name &&
        user.email &&
        user.gender &&
        user.ip_address
    ) {
        return true;
    } else {
        return false;
    }
}

const getAllUsers = () => {
    console.log("getAllUsers");
    return UsersRepo.getAllUsers();
};

const getUserById = (id) => {
    console.log("getUserById");
    if (id) {
        return UsersRepo.getUserById(id);
    } else {
        return null;
    }
};

const addUser = (user) => {
    console.log("addUser");
    if (isUser(user)) {
        return UsersRepo.addUser(user);
    } else {
        return null;
    }
}

const updateUser = (user, id) => {
    console.log("updateUser");
    if (isUser(user) && id && user.id == id) {
        return UsersRepo.updateUser(user, id);
    } else {
        return null;
    }
}

const deleteUser = (id) => {
    console.log("deleteUser");
    if (id) {
        return UsersRepo.deleteUser(id);
    } else {
        return null;
    }
}

module.exports = {
    getAllUsers,
    getUserById,
    addUser,
    updateUser,
    deleteUser
};