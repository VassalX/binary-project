FROM node:6-alpine

COPY assets ./dist
COPY bin ./bin
COPY middlewares ./middlewares
COPY repositories ./repositories
COPY routes ./routes
COPY services ./services
COPY app.js ./
COPY package*.json ./
RUN npm install

CMD ["npm","start"]
EXPOSE $PORT